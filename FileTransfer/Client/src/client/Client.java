package client;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by scriptify on 26.02.2017.
 */
public class Client {

    private String filePath;
    private String ip;
    private int port;
    private static final int TEMP_SIZE = 6022386;

    public Client(String ip, int port, String path) {
        this.filePath = path;
        this.ip = ip;
        this.port = port;
        this.execute();
    }

    private void execute() {
        Socket socket;
        int byteNum;
        int currentByteNum = 0;

        try {
            socket = new Socket(this.ip, this.port);
            OutputStream oStream = socket.getOutputStream();
            oStream.write((this.filePath + "\n").getBytes("UTF-8"));
            InputStream stream = socket.getInputStream();


            byte[] fileArr = new byte[Client.TEMP_SIZE];

            do {
                byteNum = stream.read(fileArr, currentByteNum, (fileArr.length - currentByteNum));
                if(byteNum > 0)
                    currentByteNum += byteNum;
            } while(byteNum > -1);

            if(currentByteNum == 1) {
                switch(fileArr[0]) {
                    case 127:
                        System.out.println("Error: Invalid path!");
                        break;
                    case 126:
                        System.out.println("Couldn't read file.");
                        break;
                }
                socket.close();
                return;
            }

            // Calculate output filename
            String fileName = "downloadfile-" + System.currentTimeMillis();
            String[] ext = this.filePath.split("\\.");
            if(ext.length > 0)
                fileName += "." + ext[ext.length - 1];

            try {
                byte[] newFileArr = new byte[currentByteNum];
                // Copy into smaller array with actual size
                for(int i = 0; i < currentByteNum; i++) {
                    newFileArr[i] = fileArr[i];
                }
                Files.write(Paths.get("files/" + fileName), newFileArr);
            } catch(IOException e) {
                e.printStackTrace();
                System.out.println("Cannot save file!");
            }

            System.out.println("Downloaded file: " + fileName + " (" + currentByteNum + " bytes)");
            socket.close();
        } catch (IOException e) {
            System.out.println("Connection with " + this.ip + ":" + this.port + " failed");
        }
    }

}
