# Java Fileserver

## Allgemein
Der Code für den Server befindet sich im Ordner "Server", der für den Client unter "Client".
Die ausführbaren jar-Dateien befinden sich im Ordner "exec".

## Ausführen
Um den Server zu starten, einfach die Datei "FileTransferServer.jar" über ein CLI öffnen.
Um den Client zu starten, einfach die Datei "Client.jar" über ein CLI öffnen.
## Funktionsweise
Sobald der Server gestartet wird, wartet er auf Port 6666 auf eingehende Verbindungen. Der Server stellt alle Dateien im Ordner "./files" zur Verfügung.
Der Client muss mit folgenden Argumenten aufgerufen werden:
```java
<ip> <port> <filename>
```

Um nun z.B. ein Bild herunterzuladen, müsste man Folgendes angeben:
```bash
java -jar localhost 6666 image.jpg
```

Die Datei wird dann auf dem Client unter "./files" abgespeichert.
