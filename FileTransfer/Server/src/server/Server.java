package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by scriptify on 14.02.2017.
 */
public class Server {

    private int port;
    private boolean isRunning = true;

    public Server(int port) {
        this.port = port;
        this.listen();
    }

    private void listen() {
        ServerSocket server = null;
        ExecutorService pool = Executors.newCachedThreadPool();

        try {
            server = new ServerSocket(this.port);
        } catch (IOException e) {
            System.out.println("Cannot create Socket.");
            return;
        }

        while(this.isRunning) {
            try {
                Socket client = server.accept();
                ServerCallable clientThread = new ServerCallable(client);
                pool.submit(clientThread);

            } catch (IOException e) {
                System.out.println("Cannot establish connection.");
            }
        }

        try {
            server.close();
            pool.shutdown();
        } catch (IOException e) {
            System.out.println("Cannot shutdown server.");
        }
    }

    public void shutdown() {
        this.isRunning = false;
    }

}
