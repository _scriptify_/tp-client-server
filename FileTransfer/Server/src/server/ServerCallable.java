package server;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

/**
 * Created by scriptify on 14.02.2017.
 */
public class ServerCallable implements Callable {
    private Socket socket;
    private boolean isRunning = true;
    private OutputStream toClient;


    public ServerCallable(Socket s) {
        this.socket = s;
    }

    private void writeError(String type) {
        byte[] errorArr = new byte[1];
        switch(type) {
            case "invalid-path":
                errorArr[0] = 127;
                break;
            case "read-error":
                errorArr[0] = 126;
                break;
        }

        try {
            this.toClient.write(errorArr);
            this.toClient.flush();
        } catch (IOException e) {
            System.out.println("Couldn't send message to client.");
        }

    }

    public Boolean call() {
        BufferedReader fromClient = null;

        try {
            fromClient = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.toClient = socket.getOutputStream();
        } catch (IOException e) {
            System.out.println("Cannot establish connection.");
        }

        String fileName = null;


        System.out.println("Client connected.");

        try {
            fileName = fromClient.readLine();
            System.out.println("Transferring " + fileName);
            Path fPath = null;
            byte[] fileArr = null;

            try {
                fPath = Paths.get(fileName);
            } catch(InvalidPathException e) {
                System.out.println("Client transmitted invalid filepath: " + fileName);
                this.writeError("invalid-path");
            }

            if(fPath != null) {
                try {
                    fileArr = Files.readAllBytes(fPath);
                } catch(IOException e) {
                    this.writeError("read-error");
                }
            }

            if(fileArr != null) {
                this.toClient.write(fileArr, 0, fileArr.length);
            }


        } catch (IOException e) {
            System.out.println("Error during client-server communication.");
        }

        try {
            this.toClient.close();
            fromClient.close();
            this.socket.close();
        } catch (IOException e) {
            System.out.println("Couldn't close connection.");
        }

        return true;

    }

    public void exit() {
        this.isRunning = false;
    }

}
