# Server-Rechner

## Allgemein
Der Code für den Server befindet sich im Ordner "WebsiteDownload", der für den Client unter "SiteLoaderClient".
Die ausführbaren jar-Dateien befinden sich im Ordner "exec".

## Ausführen
Um den Client zu starten, einfach die Datei "LoaderClient.jar" öffnen.
Um den Server zu starten, einfach die Datei "Server.jar" über ein CLI öffnen.

## Dokumentation der Funktionsweise

### Server ausführen
Öffnen Sie ein CLI und starten Sie den Server folgendermaßen:

![cli-server](img/server.png)

Der Server hört nun auf den Port 8080 und ist bereit für Anfragen. Benachrichtigunen werden in das CLI-Fenster geloggt.

### Client ausführen
Sobald eine Serverinstanz gestartet wurde, können Sie sich mit dem Client hinverbinden. Dazu einfach die Datei "LoaderClient.jar" öffnen.

Am Anfang müssen IP und Port des Servers angegeben werden. Auf dem lokalen Rechner passen die bereits eingetragenen Werte und Sie können sich sofort hinverbinden.

![connect](img/connect.png)

Sobald Sie sich zum Server hinverbunden haben, erscheint ein Dialog, wo die URL einer Website eingegeben werden kann. Sobald Sie bestätigen, wird dem Server die URL gesendet, welcher dann auf zweierlei Art reagieren kann:
1. Die Website downloaden, lokal abspeichern und dem Client senden.
2. Die lokal abgespeicherte Seite dem Client senden.

![app](img/app.png)

Der Einfachkeit wegen wird die gesendete Seite nicht angezeigt, aber Sie kommt an und man könnte weiter damit verfahren. Jedoch hätte diese Aufgabe nur richtig Sinn, wenn auch alle Dateien, welche mit der Website eingebunden wurden (d.h. js-Dateien, css-Dateien etc.) mit heruntergeladen würden und deren Pfade dann zu lokalen aufgelöst werden würden (ansonste macht das Anzeigen der Seite nicht viel Sinn).


## Protokoll
![protokoll](img/Proxy.png)

## Welchen Vorteil hat dieses Vorgehen und welche Aufgabe (als Netzwerkkomponente) erfüllt der Server?
Dieses Vorgehen hat den Vorteil, dass Websites die bereits besucht worden sind, gecached werden können. Somit verringert man die Ladezeit um ein Vielfaches und bestimmte Websites könnten auch ohne Internetverbindung aufgerufen werden. Außerdem könnten Netzadmins bestimmte Websites sperren oder Inhalte manipulieren und filtern. Somit entspräche dieses Programm in ausgefeilter Form genau der Definition eines Proxy-Servers.
