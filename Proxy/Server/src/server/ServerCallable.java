package server;

import SiteLoader.SiteLoader;
import protocol.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

/**
 * Created by maximilian on 22/04/16.
 */
public class ServerCallable implements Callable {

    private Socket socket;
    private boolean isRunning = true;
    private ObjectOutputStream toClient;
    private Server mainThread;
    private SiteLoader siteLoader;

    public ServerCallable(Socket s, Server mainThread) {
        this.socket = s;
        this.mainThread = mainThread;
        this.siteLoader = new SiteLoader();
    }

    public Boolean call() {
        ObjectInputStream fromClient = null;

        ArrayList<String> params = new ArrayList<String>();
        params.add(this.socket.getInetAddress().toString());
        this.notifyServer(new Message("connected", params));

        try {
            fromClient = new ObjectInputStream(this.socket.getInputStream());
            this.toClient = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Message currMsg = null;


        try {
            while((currMsg = (Message) fromClient.readObject()) != null && this.isRunning) {
                this.cmd(currMsg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            this.toClient.close();
            fromClient.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.notifyServer(new Message("disconnect"));
        return false;
    }

    public void cmd(Message cmd) {


        switch(cmd.getCmd()) {
            /* intern commands */
            case "exit":
                this.exit();
                break;
            /* commands to main thread */

            case "request":
                String url = cmd.getParam(1);
                String html = null;
                try {
                    html = this.siteLoader.load(url);
                } catch (IOException e) {
                    ArrayList<String> params = new ArrayList<>();
                    params.add("load-error");
                    Message sendMsg = new Message("error", params);
                    this.sendToClient(sendMsg);
                }
                ArrayList<String> params = new ArrayList<>();
                params.add(html);
                Message sendMsg = new Message("response", params);
                this.sendToClient(sendMsg);
                break;

            default:
                this.notifyServer(cmd);
        }
    }

    public void sendToClient(Message msg) {
        try {
            System.out.println("To client: " + msg.getCmd());
            this.toClient.writeObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exit() {
        this.isRunning = false;
    }

    private void notifyServer(Message msg) {
        this.mainThread.update(msg);
    }

}
