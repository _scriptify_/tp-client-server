package SiteLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sttormax on 23.01.2017.
 */
public class SiteLoader {

    public String loadAsString(String urlU) throws IOException {
        URL url = null;
        StringBuilder sb = new StringBuilder();

        try {
            url = new URL(urlU);
        } catch(MalformedURLException e) {
            e.printStackTrace();
        }


        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
            for (String line; (line = reader.readLine()) != null;) {
                sb.append(line);
            }
        }

        return sb.toString();
    }

    public String loadAndSave(String url) throws IOException {
        String html = this.loadAsString(url);
        List<String> lines = Arrays.asList(html);
        Path file = Paths.get(this.urlToFilename(url));
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return html;
    }

    public String load(String url) throws IOException {
        Path path = Paths.get(this.urlToFilename(url));
        //System.out.println(path.toAbsolutePath()));

        if(Files.exists( path )) {

            List<String> lines = null;

            try {
                lines = Files.readAllLines(path);
            } catch (IOException e) {
                e.printStackTrace();
            }

            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < lines.size(); i++)
                sb.append( lines.get(i) );

            return sb.toString();

        }

        return this.loadAndSave(url);

    }

    private String urlToFilename(String url) {
        return "cache/" + url.replace("/", "-").replace(":", "-") + ".html";
    }

    public void clearCache() {
        File index = new File("cache/");

        String[]entries = index.list();

        for(String s: entries){
            File currentFile = new File(index.getPath(),s);
            currentFile.delete();
        }
    }

}
