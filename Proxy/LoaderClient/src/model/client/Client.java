package model.client;

import model.BrowserBridge;
import protocol.Message;

import java.io.*;
import java.net.Socket;
import java.util.function.Function;

/**
 * Created by maximilian on 22/04/16.
 */
public class Client {
    private String host;
    private int port;
    private Socket socket;
    private ObjectInputStream fromServer;
    private ObjectOutputStream toServer;
    private Function<Message, Void> gotFromServer;
    private Function<String, Void> errorToClient;
    private Reader reader;
    private Writer writer;

    public Client(String host, int port, Function<Message, Void> gotFromServer, Function<String, Void> error) {
        this.host = host;
        this.port = port;
        this.gotFromServer = gotFromServer;
        this.errorToClient = error;
        this.connect();

        Function<String, Void> errorO = (String err) -> {
            this.closedConnection();
            error.apply(err);
            return null;
        };

        this.reader = new Reader(this.fromServer, gotFromServer, errorO);
        this.writer = new Writer(this.toServer);
    }

    private void closedConnection() {
        try {
            this.toServer.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connect() {
        try {
            socket = new Socket(this.host, this.port);
            toServer = new ObjectOutputStream(socket.getOutputStream());
            fromServer = new ObjectInputStream(socket.getInputStream());
        } catch (Exception e) {
            this.errorToClient.apply("Could not connect to the server. Please try another IP/port.");
        }
    }

    public void send(Message msg) {
        this.writer.writeToServer(msg);
    }
}
