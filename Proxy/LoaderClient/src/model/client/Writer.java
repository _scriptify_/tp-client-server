package model.client;

import protocol.Message;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by maximilian on 26/04/16.
 */
public class Writer {
    private ObjectOutputStream toServer;
    private ExecutorService executorService;

    public Writer(ObjectOutputStream toServer) {
        this.toServer = toServer;
        this.executorService = Executors.newCachedThreadPool();
    }

    public void writeToServer(Message msg) {
        this.executorService.submit(() -> {
            try {
                this.toServer.writeObject(msg);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
