var $userInput = document.getElementById('userInput');
var $loadBtn = document.getElementById('loadSite');
var $out = document.getElementById('out');
var $app = document.getElementById('app');

function update(what, cmd) {
	switch(what) {
		case 'server':
		
			var out = cmdToHTML(cmd);
			$out.innerHTML = out;
		break;
		
		case 'error':
		break;
	}
}

function cmdToHTML(cmd) {
	var out = cmd.cmd + ': <br />';
	cmd.params.forEach(function(p) {
		out += p + '<br />';
	});
	
	return out;
}

$loadBtn.onclick = function(e) {
    var url = $userInput.value;
    if(url !== '') {
        window.browserBridge.loadSite(url);
    }
}