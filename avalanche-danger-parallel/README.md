# Paralleler Server in C

## Allgemein
Der Code für Server und Client befinden sich im Ordner "src".
Die ausführbaren Dateien befinden sich im Ordner "exec".

## Funktionsweise
Der Server kann folgendermaßen gestartet werden:
```bash
./server <port>
```

Der Client kann folgendermaßen gestartet werden:
```bash
./client <ip> <port>
```

Sobald sich der Client zum Server hinverbunden hat, kann der Nutzer entweder einen Ortsnamen senden, zu dem sie/er genauere Informationen über die Lawinenlage hätte, oder die Orte auflisten.

Im Unteschied zur vorigen Aufgabe arbeitet der Server die Abfragen hier parallel in eigenen Threads ab. Somit ist klar, dass die Vorgänge für die einzelnen Clients schneller gehen (insofern diese auf eine Freigabe des Server warten müssten).
