/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <pthread.h>

char* readFile(char* path) {
    // Read from file
    FILE *f = fopen(path, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *string = malloc(fsize + 1);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = 0;
    return string;
}

 int isSubstring(char * haystack, char * needle) {
     int i = 0;
     int d = 0;
     if (strlen(haystack) >= strlen(needle)) {
         for (i = strlen(haystack) - strlen(needle); i >= 0; i--) {
             int found = 1;
             for (d = 0; d < strlen(needle); d++) {
                 if (haystack[i + d] != needle[d]) {
                     found = 0; 
                     break;
                 }
             }
             if (found == 1) {
                 return i;
             }
         }
         return -1;
     } else {
         //fprintf(stdout, "haystack smaller\n"); 
     }
 }

 
char* copySubstr(char* srcStr, char* beginStr, char* endStr) {
    int beginPos = isSubstring(srcStr, beginStr) + strlen(beginStr);
    int endPos = isSubstring(srcStr, endStr);
    
    char* retStr = malloc (sizeof (char) * (endPos - beginPos  + 1));
    strncpy(retStr, srcStr + beginPos, endPos - beginPos);
    
    return retStr;
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

char* join_strings(char* strings[], char* seperator, int count) {
    char* str = NULL;             /* Pointer to the joined strings  */
    size_t total_length = 0;      /* Total length of joined strings */
    int i = 0;                    /* Loop counter                   */

    /* Find total length of joined strings */
    for (i = 0; i < count; i++) total_length += strlen(strings[i]);
    total_length++;     /* For joined string terminator */
    total_length += strlen(seperator) * (count - 1);// for seperators

    str = (char*) malloc(total_length);  /* Allocate memory for joined strings */
    str[0] = '\0';                      /* Empty string we can append to      */

    /* Append all the strings */
    for (i = 0; i < count; i++) {
        strcat(str, strings[i]);
        if (i < (count - 1)) strcat(str, seperator);
    }

    return str;
}


struct threadArgs {
    char* buffer;
    int newsockfd;
    int bufferLen;
    char** locations;
    int* danger;
    char* content;
};

void* threadBody(void* arguments) {
    
    struct threadArgs *args = (struct threadArgs*)arguments;
    
    int n = 0;
    
    bzero(args->buffer, args->bufferLen);
        
        n = read(args->newsockfd,args->buffer, args->bufferLen - 1);
        
        if (n < 0) error("ERROR reading from socket");
        
        printf("Received command: %s\n",args->buffer);
        // Now react to it

        char* msg = "No such command!\n";
        int done = 0;
        
        if(memcmp( args->buffer, "ls", strlen( "ls" ) ) == 0) {
            msg = join_strings(args->locations, "\n", 8);
            done = 1;
        } else {
            // Perform long calculation
            /*
            int i = 0;
            for(i = 0; i < sizeof(args->locations); i++) {
                if(memcmp( args->buffer, args->locations[i], strlen( args->locations[i] ) ) == 0) {
                    done = 1;
                    char* format = "\t\t\n\n%s\n\nWichtig: %s\n\nWarnungen: %s\n\nSchneestruktur: %s\n\nReisehinweise: %s\n\nInsgesamte Bewertung der Lawinengefahr: %d%\n\n";
                    msg = (char*) malloc(5000 * sizeof(char));
                    sprintf(msg, format, 
                            args->locations[i],
                            copySubstr(args->content, "<caaml:highlights>", "</caaml:highlights>"),
                            copySubstr(args->content, "<caaml:wxSynopsisComment>", "</caaml:wxSynopsisComment>"),
                            copySubstr(args->content, "<caaml:snowpackStructureComment>", "</caaml:snowpackStructureComment>"),
                            copySubstr(args->content, "<caaml:travelAdvisoryComment>", "</caaml:travelAdvisoryComment>"),
                            args->danger[ i ]
                           );
                    break;
                }
            }*/
            char* format = "Lawinengefahr: %d%\n";
            msg = (char*) malloc(100 * sizeof(char));
            sprintf(msg, format, rand() % 101);
        }
        
        
        
        n = write(args->newsockfd, msg, strlen(msg));
        if(done == 1)
            free(msg);
        if (n < 0) error("ERROR writing to socket");
        pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    
    struct threadArgs arguments;
    
    srand(time(NULL));
    system("wget https://lawine.tirol.gv.at/rest/bulletin/latest/xml/de -O danger.xml");
    
    char* content = readFile("danger.xml");
    
    system("rm danger.xml; clear;");
    
    char* locations[8];
    locations[0] = "Vorarlberg";
    locations[1] = "Tirol";
    locations[2] = "Kärnten";
    locations[3] = "Steiermark";
    locations[4] = "Salzburg";
    locations[5] = "Oberösterreich";
    locations[6] = "Niederösterreich";
    locations[7] = "Burgenland";
    
    arguments.locations = locations;
    
    int danger[8] = { rand() % 101, rand() % 101, rand() % 101, rand() % 101, rand() % 101, rand() % 101, rand() % 101, rand() % 101 };
    
    arguments.danger = danger;
    arguments.content = content;
    
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     int bufferLen = 4096;
     char buffer[bufferLen];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     
     arguments.buffer = buffer;
     arguments.bufferLen = bufferLen;
     
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        error("ERROR on binding");
     
     listen(sockfd,5);
     
     clilen = sizeof(cli_addr);
     
     printf("Downloaded avalanche data, waiting for clients...\n");
     while(1) {
        pthread_t thread;
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        arguments.newsockfd = newsockfd;
        
        if (newsockfd < 0) 
            error("ERROR on accept");
        
        if(pthread_create(&thread, NULL, &threadBody, (void *) &arguments) != 0) {
            error("Couldn't create thread!");
        }
        
        
        
        
    }
    
    close(newsockfd);
    close(sockfd);
    return 0; 
}