package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by scriptify on 14.02.2017.
 */
public class Server {

    private int port;
    private boolean isRunning = true;

    public Server(int port) {
        this.port = port;
        this.listen();
    }

    private void listen() {
        ServerSocket server = null;

        try {
            server = new ServerSocket(this.port);
        } catch (IOException e) {
            System.out.println("Cannot create Socket.");
            return;
        }

        while(this.isRunning) {
            try {
                Socket client = server.accept();
                BufferedReader fromClient = new BufferedReader( new InputStreamReader(client.getInputStream()) );
                String clientStr = fromClient.readLine();

                System.out.println("Client connected. Client service: " + clientStr);

                switch(clientStr) {
                    case "get-random":
                        client.getOutputStream().write("Calculating a very random number. This could take some time...\n".getBytes("UTF-8"));
                        try {
                            Thread.sleep(5 * 1000);
                        } catch (InterruptedException e) {
                            System.out.println("Thread can't sleep :( Give him some valium...");
                        }
                        client.getOutputStream().write("The result is: 42. Contingency: 0.97/1\n".getBytes("UTF-8"));
                        client.getOutputStream().write("END\n".getBytes("UTF-8"));
                        break;

                    case "get-time":
                        client.getOutputStream().write("Calculating the actual time + how much the universe weighs. This could take some time...\n".getBytes("UTF-8"));
                        try {
                            Thread.sleep(5 * 1000);
                        } catch (InterruptedException e) {
                            System.out.println("Thread can't sleep :( Give him some valium...");
                        }
                        client.getOutputStream().write(("Actual time couldn't be calculated, but the universe weighs " + System.currentTimeMillis() + "% as much as you.\n").getBytes("UTF-8"));
                        client.getOutputStream().write("END\n".getBytes("UTF-8"));
                        break;

                    default:
                        client.getOutputStream().write("No such command!\n".getBytes("UTF-8"));
                        client.getOutputStream().write("END\n".getBytes("UTF-8"));
                        break;
                }

                System.out.println("Sent result.");

            } catch (IOException e) {
                System.out.println("Connection failed.");
            }
        }

        try {
            server.close();
        } catch (IOException e) {
            System.out.println("Cannot shutdown server.");
        }
    }

    public void shutdown() {
        this.isRunning = false;
    }

}
