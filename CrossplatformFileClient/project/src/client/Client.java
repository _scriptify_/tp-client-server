package client;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by scriptify on 26.02.2017.
 */
public class Client {

    private String filePath;
    private String ip;
    private Socket socket;
    private int port;

    public Client(String ip, int port, String path) {
        this.filePath = path;
        this.ip = ip;
        this.port = port;
        this.execute();
    }

    private void close() {
        try {
            this.socket.close();
        } catch (IOException e) {
            System.out.println("Could not close the connection.");
        }
    }

    private void execute() {

        try {
            this.socket = new Socket(this.ip, this.port);
            OutputStream oStream = this.socket.getOutputStream();
            oStream.write(("GET " + this.filePath + "\n").getBytes("UTF-8"));

            BufferedReader reader = new BufferedReader( new InputStreamReader(this.socket.getInputStream()) );

            String line = reader.readLine();

            if(line.contains("ERROR")) {
                switch (line) {
                    case "ERROR 1":
                        System.out.println("Invalid command format!");
                        break;
                    case "ERROR 3":
                        System.out.println("This file doesn't exist or has an invalid name-format!");
                        break;
                    default:
                        System.out.println("Unknown error. Please contact mission control for further information.");
                        break;
                }
                this.close();
                return;
            }

            // Calculate output filename
            String fileName = "downloadfile-" + System.currentTimeMillis();
            String[] ext = this.filePath.split("\\.");
            if(ext.length > 0)
                fileName += "." + ext[ext.length - 1];

            try {
                /*ArrayList<String> lines = new ArrayList<>();
                lines.add(line);*/
                Files.write(Paths.get("files/" + fileName), line.getBytes());
            } catch(IOException e) {
                System.out.println("Cannot save file!");
            }

            System.out.println("Downloaded file: " + fileName);
        } catch (IOException e) {
            System.out.println("Connection with " + this.ip + ":" + this.port + " failed");
        }
    }

}
