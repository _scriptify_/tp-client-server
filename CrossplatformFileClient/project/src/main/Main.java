package main;

import client.Client;

/**
 * Created by scriptify on 26.02.2017.
 */
public class Main {
    public static void main (String [] args ) {
        if(args.length < 3) {
            System.out.println("Please specify 3 arguments: <ip> <port> <filename>");
            return;
        }

        Client c = new Client(args[0], Integer.parseInt(args[1]), args[2]);
    }
}
