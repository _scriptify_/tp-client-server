# Java fileserver client

## Allgemein
Der Code befindet sich im Ordner "project", die ausführbare jar-Datei im Ordner "exec".

## Funktionsweise
Sobald der Server gestartet wurde, können folgendermaßen Datein heruntergeladen werden:
```bash
java -jar CrossplatformFileClient.jar <ip> <port> <filename>
```

Diese Beispiel zeigt, dass es im Grunde keine Rolle spielt, auf welchem Betriebssystem der Server/Client läuft, solang diese sich an dasselbe Protokoll halten.
