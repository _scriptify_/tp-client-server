package server;

/**
 * Created by sttormax on 16.01.2017.
 */
public class User {

    private String username;
    private String password;
    private boolean isUserAuthenticated;

    public User(String username, String pw) {
        this.username = username;
        this.password = pw;
    }

    public User() {
        this.username = "";
        this.password = "";
    }

    public void authenticate(String pw) {
        this.isUserAuthenticated = (!this.username.equals("")) && this.password.equals(pw);
    }

    public boolean isAuthenticated() {
        return this.isUserAuthenticated;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
