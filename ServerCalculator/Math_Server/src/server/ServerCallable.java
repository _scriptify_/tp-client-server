package server;

import math.MathMethods;
import protocol.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by maximilian on 22/04/16.
 */
public class ServerCallable implements Callable {

    private Socket socket;
    private boolean isRunning = true;
    private ObjectOutputStream toClient;
    private Server mainThread;
    private User user = new User();

    public ServerCallable(Socket s, Server mainThread) {
        this.socket = s;
        this.mainThread = mainThread;
        this.user.setPassword("javaiscool");
    }

    public Boolean call() {
        ObjectInputStream fromClient = null;

        ArrayList<String> params = new ArrayList<String>();
        params.add(this.socket.getInetAddress().toString());
        this.notifyServer(new Message("connected", params));

        try {
            fromClient = new ObjectInputStream(this.socket.getInputStream());
            this.toClient = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Message currMsg = null;


        try {
            while((currMsg = (Message) fromClient.readObject()) != null && this.isRunning) {
                this.cmd(currMsg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            this.toClient.close();
            fromClient.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.notifyServer(new Message("disconnect"));
        return false;
    }

    public void cmd(Message cmd) {

        if(cmd.getCmd().equals("login")) {
            String username = cmd.getParam(1);
            String password = cmd.getParam(2);
            this.user.setUsername(username);
            this.user.authenticate(password);
            System.out.println("" + username + "  " + password);
            if(this.user.isAuthenticated()) {
                ArrayList<String> params = new ArrayList<>();
                params.add("success");
                Message msg = new Message("authentication", params);
                this.sendToClient(msg);
                return;
            }
        }

        if(!this.user.isAuthenticated()) {
            ArrayList<String> params = new ArrayList<>();
            params.add("failed");
            Message msg = new Message("authentication", params);
            this.sendToClient(msg);
            return;
        }

        switch(cmd.getCmd()) {
            /* intern commands */
            case "exit":
                this.exit();
                break;
            /* commands to main thread */

            case "add":
                ArrayList<String> params = new ArrayList<>();
                params.add(MathMethods.add( Integer.parseInt(cmd.getParam(1)), Integer.parseInt(cmd.getParam(2)) ) + "");
                Message msg = new Message("result", params);
                this.sendToClient(msg);
                break;

            case "subtract":
                ArrayList<String> params1 = new ArrayList<>();
                params1.add(MathMethods.subtract( Integer.parseInt(cmd.getParam(1)), Integer.parseInt(cmd.getParam(2)) ) + "");
                Message msg1 = new Message("result", params1);
                this.sendToClient(msg1);
                break;

            case "multiply":
                ArrayList<String> params2 = new ArrayList<>();
                params2.add(MathMethods.multiply( Integer.parseInt(cmd.getParam(1)), Integer.parseInt(cmd.getParam(2)) ) + "");
                Message msg2 = new Message("result", params2);
                this.sendToClient(msg2);
                break;

            case "divide":
                ArrayList<String> params3 = new ArrayList<>();
                params3.add(MathMethods.divide( Integer.parseInt(cmd.getParam(1)), Integer.parseInt(cmd.getParam(2)) ) + "");
                Message msg3 = new Message("result", params3);
                this.sendToClient(msg3);
                break;

            default:
                this.notifyServer(cmd);
        }
    }

    public void sendToClient(Message msg) {
        try {
            System.out.println("To client: " + msg.getCmd());
            this.toClient.writeObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exit() {
        this.isRunning = false;
    }

    private void notifyServer(Message msg) {
        this.mainThread.update(msg);
    }

}
