package server;

import protocol.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by maximilian on 22/04/16.
 */
public class Server {
    private int port;
    private boolean isRunning = true;
    private int counter = 0;
    private ArrayList<ServerCallable> clientThreads;

    public Server(int port) {
        this.port = port;
        this.clientThreads = new ArrayList<>();
        this.listenForClients();
    }

    public void listenForClients() {
        ServerSocket server = null;
        ExecutorService pool = Executors.newCachedThreadPool();

        try {
            server = new ServerSocket(this.port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(this.isRunning) {
            try {
                Socket client = server.accept();
                ServerCallable clientThread = new ServerCallable(client, this);
                pool.submit(clientThread);

                clientThreads.add(clientThread);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            server.close();
            pool.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update(Message cmd) {
        System.out.println("Client: " + cmd.getCmd());
        this.cmd(cmd);
    }

    private void shutdown() {
        this.isRunning = false;
    }

    private void broadcast(Message msg) {
        for(ServerCallable client : this.clientThreads) {
            client.sendToClient(msg);
        }
    }

    private void cmd(Message cmd) {
        switch(cmd.getCmd().trim().toLowerCase()) {
            case "shutdown":
                this.shutdown();
                break;
            case "broadcast":
                this.broadcast(cmd);
                break;
            case "connected":
                System.out.println("Client connected: " + cmd.getParam(1));
                break;
        }
    }
}
