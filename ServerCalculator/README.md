# Server-Rechner

## Allgemein
Der Code für den Server befindet sich im Ordner "MathServer", der für den Client unter "MathClient".
Die ausführbaren jar-Dateien befinden sich im Ordner "exec".

## Ausführen
Um den Client zu starten, einfach die Datei "MathClient.jar" öffnen.
Um den Server zu starten, einfach die Datei "Math_Server.jar" über ein CLI öffnen.

## Dokumentation der Funktionsweise

### Server ausführen
Öffnen Sie ein CLI und starten Sie den Server folgendermaßen:

![cli-server](img/start-server.png)

Der Server hört nun auf den Port 8080 und ist bereit für Anfragen. Benachrichtigunen werden in das CLI-Fenster geloggt.

### Client ausführen
Sobald eine Serverinstanz gestartet wurde, können Sie sich mit dem Client hinverbinden. Dazu einfach die Datei "MathClient.jar" öffnen.

Am Anfang müssen IP und Port des Servers angegeben werden. Auf dem lokalen Rechner passen die bereits eingetragenen Werte und Sie können sich sofort hinverbinden.

![connect](img/connect.png)

Sobald Sie sich zum Server verbunden haben, erscheint der Authentifizierungsdialog. Um sich zu authentifizieren können Sie den Standardwert des Nutzernamens gleich lassen. Tragen Sie "javaiscool" als Passwort ein und klicken Sie auf "Authenticate".

![auth](img/auth.png)

Sollten falsche Nutzerdaten eingegeben werden, erschein folgende Ausgabe:

![fail](img/fail.png)

Bei richtigen Nutzerdaten gelangen Sie zur eigentlichen Applikation:

![app](img/app.png)

Um nun Berechnungen durchführen zu können, tippen Sie einfach Rechungen im folgenden Format ein:
__[Zahl 1] [Rechenzeichen] [Zahl 2]__
z.B. 2 + 4

Der Client erkennt automatisch, sobald Sie eine Rechnung eingetippt haben und schickt die Berechnung an den Server. Dieser schickt dann das Ergebnis zurück, welches dann umgehend angezeigt wird.

![calc](img/calc.png)

## Protokoll

### Bei falschen Nutzerdaten
![wrong](img/protocol.png)

### Bei korrekten Nutzerdaten
![correct](img/protocol-s.png)
