var $userInput = document.getElementById('userInput');
var $out = document.getElementById('out');
var $app = document.getElementById('app');

function update(what, cmd) {
	switch(what) {
		case 'server':
		
			var out = cmdToHTML(cmd);
			$out.innerHTML = out;

			if(cmd.cmd === 'authentication') {
			    var $login = document.getElementById('login');
			    if(cmd.params[0] === 'success') {
			        $login.style.display = 'none';
			    } else {
			        $app.innerHTML = 'Failed to authenticate!';
			    }
			}
		
		break;
		
		case 'error':
		break;
	}
}

function cmdToHTML(cmd) {
	var out = cmd.cmd + ': <br />';
	cmd.params.forEach(function(p) {
		out += p + '<br />';
	});
	
	return out;
}

$userInput.oninput = function(e) {
	$out.innerHTML = e.target.value;
	var retObj = evaluate(e.target.value);
	if(retObj.method !== '' && retObj.num1 !== '' && retObj.num2 !== '') {
		browserBridge.calculate(retObj.method, retObj.num1, retObj.num2);
	}
}

function evaluate(inputStr) {
	var splitStr = [
		{
			sign: '+',
			method: 'add'
		},
		{
			sign: '-',
			method: 'subtract'
		},
		{
			sign: '*',
			method: 'multiply'
		},
		{
			sign: '/',
			method: 'divide'
		}
	];
	
	var method = '';
	var retObj = {
		method: '',
		num1: '',
		num2: ''
	};
	
	splitStr.forEach(function(obj){
		var splitted = inputStr.split(obj.sign);
		if(splitted.length === 2) {
			retObj.method = obj.method;
			retObj.num1 = (splitted[0] + '').trim();
			retObj.num2 = (splitted[1] + '').trim();
		}
	});
	
	return retObj;
}