package controller;

import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import model.BrowserBridge;
import netscape.javascript.JSObject;

/**
 * An UI - solution with the JavaFX WebView
 *
 * @author Maximilan Torggler
 * @version 0.1
 */
public class BrowserViewController {
    @FXML
    private WebView browserInterface;
    private WebEngine engine;
    private static final String pathToFile = "/app/index.html";
    private static BrowserBridge bridge;

    @FXML
    private void initialize() {
        this.engine = browserInterface.getEngine();
        this.bridge = new BrowserBridge(this);
        this.loadWebView();
    }

    public void loadWebView() {
        this.engine.load( getClass().getResource(pathToFile).toExternalForm() );
        //this.engine.load("http://localhost:8080");
        //this.engine.load("http://localhost:8080");
        JSObject window = (JSObject) this.engine.executeScript("window");
        window.setMember("browserBridge", this.bridge);
    }

    public WebEngine getEngine() {
        return this.engine;
    }
}
