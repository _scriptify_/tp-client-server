package model.client;

import model.BrowserBridge;
import protocol.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.function.Function;

/**
 * Created by maximilian on 26/04/16.
 */
public class Reader extends Thread {

    private ObjectInputStream fromServer;
    private Function<Message, Void> gotFromServer;
    private Function<String, Void> closedConnection;
    private boolean isRunning = true;

    public Reader(ObjectInputStream fromServer, Function<Message, Void> gotFromServer, Function<String, Void> closedConnection) {
        this.fromServer = fromServer;
        this.gotFromServer = gotFromServer;
        this.closedConnection = closedConnection;
        this.start();
    }

    public void stopReading() {
        this.isRunning = false;
    }

    public void run() {
        Message currMsg;
        try {
            while(this.isRunning && (currMsg = (Message) fromServer.readObject()) != null) {
                System.out.println("From server: " + currMsg.getCmd());
                this.gotFromServer.apply(currMsg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fromServer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        this.closedConnection.apply("The server closed the connection.");
    }

}
