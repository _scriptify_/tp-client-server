package model;

import controller.BrowserViewController;
import javafx.application.Platform;
import model.client.Client;
import protocol.Message;

import java.util.ArrayList;
import java.util.function.Function;

/**
 * Created by sttormax on 16.03.2016.
 */
public class BrowserBridge {

    private BrowserViewController controller;
    private Client chatClient;
    private String host = "127.0.0.1";
    private int port = 4242;

    public BrowserBridge(BrowserViewController controller) {
        this.controller = controller;
    }

    private void gotFromServer(Message msg) {
        //this.controller.getEngine().executeScript("window.browserBridge.update(" + msg.toJSON() + ")");
        System.out.println(msg.toJSON());
        this.controller.getEngine().executeScript("update('server', " + msg.toJSON() + ");");
    }

    private void error(String err) {
        System.out.println("Error: " + err);
        this.controller.getEngine().executeScript("update('error', '" + err + "');");
    }

    public void connect() {
        Function<Message, Void> gotFromServer = (Message msg) -> {
            Platform.runLater(() -> {
                this.gotFromServer(msg);
            });
            return null;
        };

        Function<String, Void> error = (String err) -> {
            Platform.runLater(() -> {
                this.error(err);
            });
          return null;
        };
        this.chatClient = new Client(host, port, gotFromServer, error);
        this.controller.getEngine().executeScript("update('connected');");
    }

    public void log(String msg) {
        System.out.println(msg);
    }

    public void sendCmd(String cmd, String username) {

        if(this.chatClient == null)
            return;

        this.chatClient.send(new Message(cmd, new ArrayList<>(), username));
    }

    public void calculate(String cmd, String num1, String num2) {
        if(this.chatClient == null)
            return;

        ArrayList<String> params = new ArrayList<>();
        params.add(num1);
        params.add(num2);

        this.chatClient.send(new Message(cmd, params));
    }

    public void login(String username, String pw) {
        if(this.chatClient == null)
            return;

        ArrayList<String> params = new ArrayList<>();
        params.add(username);
        params.add(pw);

        this.chatClient.send(new Message("login", params));
    }

    public void setHost(String h) {
        this.host = h;
    }

    public void setPort(int p) {
        this.port = p;
    }

    public void reload() {
        controller.loadWebView();
    }


}
