var html = '<div id="login" class="login">\
					<h1>Connect to a server</h1>\
					<input type="text" id="ip" value="127.0.0.1"/>\
					<input type="text" id="port" value="8080" />\
					<button id="connectBtn">Connect</button>\
				</div>';

var userHTML =  '<div id="login" class="login">\
               					<h1>Enter your credentials</h1>\
               					<input type="text" id="username" value="Mr. Foo Bar"/>\
               					<input type="password" id="password" value="123456789" />\
               					<button id="loginBtn">Authenticate</button>\
               				</div>';


	var $app = document.getElementById('app');
	$app.innerHTML = $app.innerHTML + html;

	var $login = document.getElementById('login');
	var $ip = document.getElementById('ip');
	var $port = document.getElementById('port');
	var $connectBtn = document.getElementById('connectBtn');

	$connectBtn.onclick = function(e) {
		window.browserBridge.setHost( $ip.value );
		window.browserBridge.setPort( $port.value );
		window.browserBridge.connect();
		$login.innerHTML = userHTML;
		var $username = document.getElementById('username');
        var $password = document.getElementById('password');
        var $loginBtn = document.getElementById('loginBtn');
        $loginBtn.onclick = function(e) {
            window.browserBridge.login($username.value, $password.value);
        }
	}