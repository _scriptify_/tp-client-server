import './login.css';

export function createLogin($app = document.getElementById('app')) {
  const markup = `
    <div class="login" id="login">
      <h1>Connect to a game server</h1>
      <input type="text" id="ip" value="127.0.0.1"/>
      <input type="text" id="port" value="8080"/>
      <button id="submit">Connect</button>
    </div>
  `;

  $app.innerHTML = $app.innerHTML + markup;
}

export function setListeners(loggedIn = () => {}) {
  const $ip = document.getElementById('ip');
  const $port = document.getElementById('port');
  const $submit = document.getElementById('submit');
  $submit.addEventListener('click', e => {
    const ip = $ip.value;
    const port = $port.value;
    window.browserBridge.setHost(ip);
    window.browserBridge.setPort(port);
    window.browserBridge.connect();
    const $login = document.getElementById('login');
    $login.style.display = 'none';
    loggedIn();
  });
}
