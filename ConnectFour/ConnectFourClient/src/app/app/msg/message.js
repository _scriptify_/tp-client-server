import './message.css';

const markup = `
  <div class="msg" id="msg-box">
    <div id="msg-close-btn" class="close-btn">Close</div>
    <div id="msg-content" class="msg-content"></div>
  </div>
`;

export function setupMsg($container = document.getElementById('app')) {
  $container.innerHTML = $container.innerHTML + markup;
}

export function setMsg({ msg = '', full = false, closeable = true }) {

  const $msgBox = document.getElementById('msg-box');
  const $msgContent = document.getElementById('msg-content');
  const $closeBtn = document.getElementById('msg-close-btn');

  $msgBox.style.display = 'block';
  $msgContent.innerHTML = msg;

  if(closeable)
    $closeBtn.style.display = 'block';
  else
    $closeBtn.style.display = 'none';

  if(full)
    $msgBox.className = 'msg full';


    document.getElementById('msg-close-btn').addEventListener('click', e => {
      document.getElementById('msg-box').style.display = 'none';
    });

}
