import { createLogin, setListeners } from './login';
import { setupMsg, setMsg } from './msg/message';
import './global.css';
import './game.css';

let updateFieldFn = () => {};
let playerNum = undefined;

createLogin();
setupMsg();

setListeners(() => {
  const { updateField } = game(document.getElementById('app'), column => {
    window.browserBridge.move(column);
  });
  updateFieldFn = updateField;
  setMsg({
    msg: 'Connected to the server.'
  });
});

function update(who, cmd) {
  if(who === 'server') {
    switch(cmd.cmd) {

      case 'broadcast':
        switch(cmd.params[0]) {
          case 'winner':
            const winner = cmd.params[1];
            setMsg({
              msg: `Player ${ winner } has won the game!`,
              full: true,
              closeable: false
            });
          break;

          case 'game-json':
            updateFieldFn(JSON.parse(cmd.params[1]));
            if(!playerNum) {
              playerNum = JSON.parse(cmd.params[2]);
            }
          break;
        }

      break;
    }
  } else if(who === 'error') {
    setMsg({
      msg: cmd,
      full: true,
      closeable: false
    });
  }
}

function game(containerElement, onColumnClick) {
  let markup = `<div id="game" class="game">`;

  for(let i = 0; i < 7; i++) {
    markup += `<div class="column" id="column-${ i }">`;
    for(let j = 0; j < 6; j++) {
      markup += `<div class="field" id="field-${ i }-${ j }"></div>`;
    }
    markup += `</div>`;
  }

  markup += `</div>`;

  containerElement.innerHTML = containerElement.innerHTML + markup;

  for(let i = 0; i < 7; i++) {
    const column = document.getElementById(`column-${ i }`);
    column.addEventListener('click', e => {
      onColumnClick(i);
    });
  }

  const updateField = field => {
    for(let i = 0; i < 7; i++) {
      for(let j = 0; j < 6; j++) {
        const elem = document.getElementById(`field-${ i }-${ j }`);
        elem.className = `field field-${ field[i][j] }`;
      }
    }
  };

  return {
    updateField
  };
}

window.update = update;
