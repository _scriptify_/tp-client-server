package protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by maximilian on 22/04/16.
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 145675678L;
    private String cmd;
    private ArrayList<String> params;
    private String username;

    public Message(String cmd, ArrayList<String> params, String username) {
        this.cmd = cmd;
        this.params = params;
        this.username = username;
    }

    public Message(String cmd, ArrayList<String> params) {
        this(cmd, params, "anon");
    }

    public Message(String cmd) {
        this(cmd, null);
    }

    public ArrayList<String> getParams() {
        return params;
    }

    public void setParams(ArrayList<String> params) {
        this.params = params;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getParam(int index) {
        if(index > 0 && index < (this.params.size() + 1))
            return this.params.get(index-1);
        return null;
    }

    public String toJSON() {
        StringBuilder sb = new StringBuilder();
        sb.append("{'cmd':'" + this.getCmd() + "'," + "'username': '" + this.username + "', 'params':[");

        for(String s : this.getParams()) {
            sb.append("'" + s +"',");
        }

        sb.delete(sb.length() - 1, sb.length() - 1);

        sb.append("]");
        sb.append("}");

        return sb.toString();
    }

    public String getUsername() {
        return this.username;
    }

}
