package ConnectFour;

/**
 * Created by sttormax on 31.01.2017.
 */
public class ConnectFour {

    private int[][] field;

    public ConnectFour() {
        this.field = new int[7][6];
    }

    public boolean move(int playerNum, int column) {
        if(playerNum > 2 || playerNum < 1 || column < 0 || column > 6)
            return false;

        if(this.field[column][0] != 0)
            return false;

        for(int i = (this.field[column].length - 1); i >= 0; i--) {
            if(this.field[column][i] == 0) {
                this.field[column][i] = playerNum;
                break;
            }
        }

        /*int[] temp = new int[this.field[column].length];

        for(int i = 0; i < this.field[column].length - 1; i++)
            temp[i + 1] = this.field[column][i];

        temp[0] = playerNum;

        this.field[column] = temp;*/


        return true;
    }

    private int hasVerticalWinner() {
        int playerNumInSerie = 0;
        int times = 0;
        for(int i = 0; i < this.field.length; i++) {
            for(int j = 0; j < this.field[i].length; j++) {
                if(playerNumInSerie != this.field[i][j]) {
                    playerNumInSerie = this.field[i][j];
                    times = 1;
                } else if(this.field[i][j] != 0) {
                    times++;
                    if(times == 4)
                        return playerNumInSerie;
                }
            }
        }

        return 0;
    }

    private int hasHorizontalWinner() {
        int playerNumInSerie = 0;
        int times = 0;
        for(int i = 0; i < this.field[0].length; i++) {
            for(int j = 0; j < this.field.length; j++) {
                int currentField = this.field[j][i];
                if(playerNumInSerie != currentField) {
                    playerNumInSerie = currentField;
                    times = 1;
                } else if(currentField != 0) {
                    times++;
                    if(times == 4)
                        return playerNumInSerie;
                }
            }
        }

        return 0;
    }

    private int hasDiagonalWinner() {
        int times = 0;
        int currPlayer = 0;

        for(int i = 0; i < this.field.length; i++) {
            for(int j = 0; j < this.field[i].length; j++) {
                // Look for each field if there's a diagonal winner

                if(this.field[i][j] != currPlayer || currPlayer == 0) {
                    currPlayer = this.field[i][j];
                    times = 1;
                }

                int verticalOffset = j + 1;

                for(int horizontalOffset = i + 1; horizontalOffset < this.field.length; horizontalOffset++) {
                    int player = 0;
                    if(verticalOffset < this.field[horizontalOffset].length) {
                        player = this.field[horizontalOffset][verticalOffset];
                    }

                    if(player == 0 || player != currPlayer) {
                        break;
                    } else {
                        times++;
                        if(times == 4)
                            return currPlayer;
                    }
                    verticalOffset++;
                }
            }
        }
        return 0;
    }

    public int hasWinner() {
        // 1. check vertical
        int vertical = this.hasVerticalWinner();
        // 2. check horizontal
        int horizontal = this.hasHorizontalWinner();
        // 3. check diagonal
        int diagonal = this.hasDiagonalWinner();

        if(horizontal != 0)
            return horizontal;

        if(vertical != 0)
            return vertical;

        if(diagonal != 0)
            return diagonal;

        return 0;
    }

    public String toJSON() {
        StringBuilder json = new StringBuilder();

        json.append("[");
        for(int i = 0; i < this.field.length; i++) {
            json.append("[");
            for(int j = 0; j < this.field[i].length; j++) {
                json.append(this.field[i][j] + "");
                if(j != (this.field[i].length - 1))
                    json.append(",");
            }

            json.append("]");
            if(i != (this.field.length - 1)) {
                json.append(",");
            }
        }

        json.append("]");
        return json.toString();
    }

    public void clear() {
        this.field = new int[7][6];
    }

}
