package server;

import ConnectFour.ConnectFour;
import protocol.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by maximilian on 22/04/16.
 */
public class Server {
    private int port;
    private boolean isRunning = true;
    private ArrayList<ServerCallable> clientThreads;
    private ConnectFour game;
    private int players = 0;
    private int currPlayer = 1;

    public Server(int port) {
        this.port = port;
        this.clientThreads = new ArrayList<>();
        this.game = new ConnectFour();
        this.listenForClients();
    }

    public void listenForClients() {
        ServerSocket server = null;
        ExecutorService pool = Executors.newCachedThreadPool();

        try {
            server = new ServerSocket(this.port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(this.isRunning) {
            try {
                Socket client = server.accept();
                ServerCallable clientThread = new ServerCallable(client, this, players + 1, this.game);
                pool.submit(clientThread);

                if(players < 2) {
                    clientThreads.add(clientThread);
                    players++;
                } else {
                    Message tooMany = new Message("no-place-left");
                    clientThread.sendToClient(tooMany);
                    clientThread.exit();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            server.close();
            pool.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getCurrPlayer() {
        return this.currPlayer;
    }

    public void setCurrPlayer(int p) {
        this.currPlayer = p;
    }

    public void update(Message cmd) {
        System.out.println("Client: " + cmd.getCmd());
        this.cmd(cmd);
    }

    private void shutdown() {
        this.isRunning = false;
    }

    private void broadcast(Message msg) {
        for(ServerCallable client : this.clientThreads) {
            client.sendToClient(msg);
        }
    }

    private void cmd(Message cmd) {
        switch(cmd.getCmd().trim().toLowerCase()) {
            case "shutdown":
                this.shutdown();
                break;
            case "broadcast":
                this.broadcast(cmd);
                break;
            case "connected":
                System.out.println("Client connected: " + cmd.getParam(1));
                break;
            case "disconnect":
                this.players--;
                if(this.players == 0) {
                    this.game.clear();
                }
                break;
        }
    }
}
