package server;

import ConnectFour.ConnectFour;
import protocol.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;

/**
 * Created by maximilian on 22/04/16.
 */
public class ServerCallable implements Callable {

    private Socket socket;
    private boolean isRunning = true;
    private ObjectOutputStream toClient;
    private Server mainThread;

    private int playerNum;
    private ConnectFour game;


    public ServerCallable(Socket s, Server mainThread, int playerNum, ConnectFour game) {
        this.socket = s;
        this.mainThread = mainThread;
        this.playerNum = playerNum;
        this.game = game;
    }

    public Boolean call() {
        ObjectInputStream fromClient = null;

        ArrayList<String> params = new ArrayList<String>();
        params.add(this.socket.getInetAddress().toString());
        this.notifyServer(new Message("connected", params));

        try {
            fromClient = new ObjectInputStream(this.socket.getInputStream());
            this.toClient = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Message currMsg = null;

        ArrayList<String> params1 = new ArrayList<>();
        params1.add("game-json");
        params1.add(this.game.toJSON());
        params1.add(this.playerNum + "");
        Message msg = new Message("broadcast", params1);
        this.sendToClient(msg);


        try {
            while((currMsg = (Message) fromClient.readObject()) != null && this.isRunning) {
                this.cmd(currMsg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            this.toClient.close();
            fromClient.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.notifyServer(new Message("disconnect"));
        return false;
    }

    public void cmd(Message cmd) {


        switch(cmd.getCmd()) {
            /* intern commands */
            case "exit":
                this.exit();
                break;
            /* commands to main thread */

            case "new-move": {
                    int column = Integer.parseInt(cmd.getParam(1));

                    if(!(this.mainThread.getCurrPlayer() == this.playerNum)) {
                        break;
                    }
                    boolean wasValidMove = this.game.move(this.playerNum, column);

                    if(this.playerNum == 1)
                        this.mainThread.setCurrPlayer(2);
                    else
                        this.mainThread.setCurrPlayer(1);

                    if(wasValidMove) {
                        int winner = this.game.hasWinner();
                        if(winner != 0) {
                            System.out.println(this.game.toJSON());
                            ArrayList<String> winnerParam = new ArrayList<>();
                            winnerParam.add("winner");
                            winnerParam.add(winner + "");
                            Message winnerMsg = new Message("broadcast", winnerParam);
                            this.notifyServer(winnerMsg);
                        } else {
                            ArrayList<String> params = new ArrayList<>();
                            params.add("game-json");
                            params.add(this.game.toJSON());
                            Message msg = new Message("broadcast", params);
                            this.notifyServer(msg);
                        }
                    } else {
                        Message invalidMoveMsg = new Message("invalid-move");
                        this.sendToClient(invalidMoveMsg);
                    }

                }
                break;


            default:
                this.notifyServer(cmd);
        }
    }

    public void sendToClient(Message msg) {
        try {
            System.out.println("To client: " + msg.getCmd());
            this.toClient.writeObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit() {
        this.isRunning = false;
    }

    private void notifyServer(Message msg) {
        this.mainThread.update(msg);
    }

}
