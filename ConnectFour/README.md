# Vier Gewinnt in Java

## Allgemein
Der Code für den Server befindet sich im Ordner "ConnectFourServer", der für den Client unter "ConnectFourClient".
Die ausführbaren jar-Dateien befinden sich im Ordner "exec".

## Ausführen
Um den Client zu starten, einfach die Datei "ConnectFourClient.jar" öffnen.
Um den Server zu starten, einfach die Datei "ConnectFourServer.jar" über ein CLI öffnen.

## Funktionsweise
Als erstes muss der Server gestartet werden.
Danach können sich zwei Teilnehmer zum Server verbinden. Der Rest läuft gemäß den Spielregeln ab und ist keine Erklärung mehr wert.
Hier ein Bild, wo sich zwei Spieler im spannenden Duell gegenüberstehen.
![app](img/app.png)
