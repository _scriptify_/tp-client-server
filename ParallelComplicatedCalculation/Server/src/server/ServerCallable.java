package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

/**
 * Created by scriptify on 26.02.2017.
 */
public class ServerCallable implements Callable {


    private Socket socket;
    private Server mainThread;

    public ServerCallable(Socket s, Server mainThread) {
        this.socket = s;
        this.mainThread = mainThread;
    }

    public Boolean call() {

        try {
            BufferedReader fromClient = new BufferedReader( new InputStreamReader(this.socket.getInputStream()) );
            String clientStr = fromClient.readLine();

            System.out.println("Client connected. Client service: " + clientStr);

            switch(clientStr) {
                case "get-random":
                    this.socket.getOutputStream().write("Calculating a very random number. This could take some time...\n".getBytes("UTF-8"));
                    try {
                        Thread.sleep(5 * 1000);
                    } catch (InterruptedException e) {
                        System.out.println("Thread can't sleep :( Give him some valium...");
                    }
                    this.socket.getOutputStream().write("The result is: 42. Contingency: 0.97/1\n".getBytes("UTF-8"));
                    this.socket.getOutputStream().write("END\n".getBytes("UTF-8"));
                    break;

                case "get-time":
                    this.socket.getOutputStream().write("Calculating the actual time + how much the universe weighs. This could take some time...\n".getBytes("UTF-8"));
                    try {
                        Thread.sleep(5 * 1000);
                    } catch (InterruptedException e) {
                        System.out.println("Thread can't sleep :( Give him some valium...");
                    }
                    this.socket.getOutputStream().write(("Actual time couldn't be calculated, but the universe weighs " + System.currentTimeMillis() + "% as much as you.\n").getBytes("UTF-8"));
                    this.socket.getOutputStream().write("END\n".getBytes("UTF-8"));
                    break;

                case "shutdown":
                    this.socket.getOutputStream().write("Server is shutting down now.\n".getBytes("UTF-8"));
                    this.socket.getOutputStream().write("END\n".getBytes("UTF-8"));
                    Executors.newSingleThreadExecutor().execute(this.mainThread::shutdown);
                    break;

                default:
                    this.socket.getOutputStream().write("No such command!\n".getBytes("UTF-8"));
                    this.socket.getOutputStream().write("END\n".getBytes("UTF-8"));
                    break;
            }
        } catch(IOException e) {
            System.out.println("Connection failed.");
        }



        System.out.println("Sent result.");
        return true;
    }
}
