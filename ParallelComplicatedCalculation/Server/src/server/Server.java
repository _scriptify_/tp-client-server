package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by scriptify on 14.02.2017.
 */
public class Server {

    private int port;
    private boolean isRunning = true;
    private ExecutorService threadPool;
    private ServerSocket serverSocket;
    private ExecutorService listenerThread = Executors.newSingleThreadExecutor();

    public Server(int port) {
        this.port = port;
        if(this.setup()) {
            this.listenerThread.execute(this::listen);
        }
    }

    private boolean setup() {
        ServerSocket server = null;
        this.threadPool = Executors.newCachedThreadPool();

        try {
            server = new ServerSocket(this.port);
        } catch (IOException e) {
            System.out.println("Cannot create Socket.");
            return false;
        }
        this.serverSocket = server;
        return true;
    }

    private void listen() {
        while(this.isRunning) {
            try {
                Socket client = this.serverSocket.accept();
                ServerCallable sc = new ServerCallable(client, this);
                threadPool.submit(sc);
            } catch (IOException e) {
                System.out.println("Connection failed.");
            }
        }
    }

    public void shutdown() {
        System.out.println("Server shutting down...");
        this.isRunning = false;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            System.out.println("Server couldn't be shutdown.");
        }
        this.listenerThread.shutdownNow();
        this.threadPool.shutdown();

        try {
            this.threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            System.out.println("Couldn't perform a clean server shutdown...");
        }

        try {
            this.serverSocket.close();
        } catch (IOException e) {
            System.out.println("Cannot shutdown server.");
        }
        System.out.println("Server has shut down.");
    }

}
