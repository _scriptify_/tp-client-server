# Paralleler Server in Java

## Allgemein
Der Code für den Server befindet sich im Ordner "Server", der für den Client unter "Client".
Die ausführbaren jar-Dateien befinden sich im Ordner "exec".

## Ausführen
Um den Client zu starten, einfach die Datei "Client.jar" über ein CLI öffnen.
Um den Server zu starten, einfach die Datei "Server.jar" über ein CLI öffnen.

## Funktionsweise
Sobald der Server gestartet wurde, nimmt dieser Anfragen auf Port 6666 entgegen.
Der Client verlangt 3 Argumente:
```bash
<ip> <port> <get-random|get-time|shutdown>
```
Das letzte Argument ist die Funktion, welche der Server ausführen soll.

__Beispiel:__
```bash
java -jar Client.jar localhost 6666 shutdown
```

Bei diesem Programm unterscheidet sich die Art, wie der Server Aufgaben abarbeitet: Nicht mehr sequenziell sondern parallel durch Threads. Somit ist klar, dass die Vorgänge für die einzelnen Clients schneller gehen (insofern diese auf eine Freigabe des Server warten müssten).

Außerdem wurde über die shutdown Funktion die Möglichkeit eines sauberen Shutdowns eingeführt.
