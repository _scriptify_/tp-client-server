package client;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by scriptify on 26.02.2017.
 */
public class Client {

    String method;
    String ip;
    int port;
    private static final int TEMP_SIZE = 6022386;

    public Client(String ip, int port, String method) {
        this.method = method;
        this.ip = ip;
        this.port = port;
        this.execute();
    }

    private void execute() {
        Socket socket;

        try {
            socket = new Socket(this.ip, this.port);
            OutputStream oStream = socket.getOutputStream();
            BufferedReader fromServer = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );

            oStream.write((this.method + "\n").getBytes("UTF-8"));

            String serverStr;
            while((serverStr = fromServer.readLine()) != null) {
                if(serverStr.equals("END")) {
                    break;
                } else {
                    System.out.println(serverStr);
                }
            }

        } catch (IOException e) {
            System.out.println("Connection with " + this.ip + ":" + this.port + " failed");
        }
    }

}
