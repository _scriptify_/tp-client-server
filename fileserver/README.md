# Fileserver in C

## Allgemein
Der Code für Server und Client befinden sich im Ordner "src".
Die ausführbaren Dateien befinden sich im Ordner "exec".

## Funktionsweise
Der Server kann folgendermaßen gestartet werden:
```bash
./server <port>
```

Sobald der Server gestartet wurde, können Datein heruntergeladen werden.
Die Nachrichten müssen dabei folgendem Format entsprechen:

```bash
GET [filename].[ext]
```

Es wurde ein Java-Client entwickelt, welcher mit diesem Server interagieren kann.
