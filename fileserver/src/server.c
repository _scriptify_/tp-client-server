/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h>

void error(const char *msg) {
    perror(msg);
    exit(1);
}

char* readFile(char* path) {
    // Read from file
    FILE *f = fopen(path, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *string = malloc(fsize + 1);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = 0;
    return string;
}

int fileExists(char *filename) {
  struct stat   buffer;   
  return (stat (filename, &buffer) == 0);
}

int main(int argc, char *argv[]) {
    
    
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     int bufferLen = 4096;
     char buffer[bufferLen];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     
     bzero((char *) &serv_addr, sizeof(serv_addr));
     
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
        error("ERROR on binding");
     
     listen(sockfd,5);
     
     clilen = sizeof(cli_addr);
     
     printf("Waiting for clients to connect...\n");
     
     while(1) {
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        
        if (newsockfd < 0) 
            error("ERROR on accept");
        
        bzero(buffer, bufferLen);
        
        n = read(newsockfd,buffer, bufferLen - 1);
        
        if (n < 0) error("ERROR reading from socket");
        
        printf("Received command: %s\n",buffer);
        // Now react to it

        char* msg = "No such command!\n";
        int done = 0;

        // GET filename.ext
        // Check command
        char* delimiter = " ";
        char* token;
        char* fname;
        int num = 0;
        int errorNum = 0;
        token = strtok(buffer, delimiter);
        
        if(memcmp( token, "GET", strlen( "GET" ) ) != 0 && num == 0) {
            errorNum = 1;
        }
        
        while(token != NULL) {
            token = strtok(NULL, delimiter);
            
            if(num == 0) {
                fname = (char*) malloc(strlen(token) + 1);
                strcpy(fname, token);
                fname[strlen(fname) - 1] = '\0';
            }
            
            num++;
        }
        
        delimiter = ".";
        token = strtok(buffer, delimiter);
        num = 0;
        
        while(token != NULL) {
            token = strtok(NULL, delimiter);
            num++;
        }
        
        if(num == 0)
            errorNum = 2;
        
        
        printf("Sending file: %s\n", fname);
        
        if(fileExists(fname)) {
            msg = readFile(fname);
        } else {
            if(errorNum == 0)
                errorNum = 3;
        }
        
        if(errorNum != 0) {
            char* format = "ERROR %d";
            msg = (char*) malloc(8 * sizeof(char));
            done = 1;
            sprintf(msg, format, errorNum);
        }
        
        strcat(msg, "\n");
        
        n = write(newsockfd, msg, strlen(msg));
        if(done == 1)
            free(msg);
        if (n < 0) error("ERROR writing to socket");
        
        
    }
    
    close(newsockfd);
    close(sockfd);
    return 0; 
}