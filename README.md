# Technologie und Planung: Übungen zur Server-Client Architektur

## Die einzelnen Ordner können so den jeweiligen Übungen zugeordnet werden (hier eine Liste)

- Übung 1: ServerCalculator
- Übung 2: ServerCalculator (hat bereits Authentifizierung)
- Übung 3: Proxy
- Übung 4: ConnectFour
- Übung 5: FileTransfer
- Übung 6: SequentialComplicatedCalculation und ParallelComplicatedCalculation
- Übung 7: ParallelComplicatedCalculation
- Übung 8: Nicht erledigt, da ich eigentlich vorhatte noch die Zusatzaufgabe zu erledigen. Ab da so viel an Dokumentation zu erstellen war, blieb mir leider beides erspart.
- Übung 9: avalanche-danger
- Übung 10: avalanche-danger-parallel
- Übung 11: fileserver und CrossplatformFileClient

## Eigen-Bewertung
Wenn es darauf ankommen würde, wie es um meine Kentnisse in der Netzwerkprogrammierung steht (d.h. in Java/C oder anderen Programmiersprachen), deren Feststellung ja eigentlich das Ziel dieser Arbeitsaufträge sein sollte, dann würde ich mich ungefähr mit einer Note von 9/10 bewerten, da ich der Ansicht bin, so gut wie jede Aufgabenstellung auf diesem Gebiet ausführen zu können. Jedoch zielen diese Aufgaben nicht wirklich gut darauf ab, die Kentnisse auf diesem Gebiet festzustellen. Dies liegt vor allem an der Notwendigkeit der sehr ausgiebigen Dokumentation jeder einzelnen Übung. Da diese eine sehr große Rolle bei der Bewertung einnimmt, wird der eigentlich wichtigere Teil, die Programmierung selbst und das Verständnis für diese Technologien, weit nach hinten verdrängt. Ich hätte bei den Aufgaben gerne mehr Mühe und Zeit investiert, aber die Tatsache, dass die Dokumentation eine so große Rolle einnimmt, hat meine Motivation stark beschränkt. Also für meine Dokumentation würde ich mir selbst höchstens 6 geben, und das hat nichts mit Faulheit zu tun, nein, ich sehen einfach nicht ein, hier so viel Aufwand zu investieren für so wenig was ich daraus lernen kann (bei der Programmierung selbst hingegen lernt man wesentlich mehr als beim monotonen verwenden von Snipping Tool und Zusammenbasteln von Diagrammen die jedesmal gleich aussehen). Ich hätte verstanden wenn die Dokumentation bei einer Übung Pflicht gewesen wäre, aber bei allen ist meiner Ansicht nach übertrieben. Dies bedeutet zusammenfassend, dass ich die Aufgaben so gelöst habe, wie ich meines Erachtens am meisten dazulernen kann, was ja schließlich dass Ziel sein sollte.
